# frozen_string_literal: true

# ISK - A web controllable slideshow system
#
# Author::    Vesa-Pekka Palmu
# Copyright:: Copyright (c) 2012-2013 Vesa-Pekka Palmu
# License::   Licensed under GPL v3, see LICENSE.md

class TemplateField < ApplicationRecord
  belongs_to :slide_template, inverse_of: :fields, class_name: "SlideTemplate"

  include RankedModel
  ranks :field_order, with_same: :slide_template_id

  scope :editable, (-> { where editable: true })
end
