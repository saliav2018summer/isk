require 'net/http'
require 'uri'
require 'open-uri'
require_relative '../cli_helpers.rb'

class SpeedrunComScheduleTask < BackgroundTask
  # Speedrun.com
  Schedule_url = "https://www.speedrun.com/speed_games_united_winter_2018/schedule"
  Schedule_id = 10


  def self.perform
    return unless Schedule_id
    say "Fetching speedrun schedule from #{Schedule_url}"
    schedule = Schedule.find Schedule_id

    data = URI.parse(Schedule_url).read
    xml = Nokogiri::Slop(data)

    schedule.schedule_events.delete_all

    # Find the schedule table
    xml.css(".singleschedule").first.css("tr").each do |row|
      next if row['class'].include?('row2')
      # There are header rows for dates
      time = row.css('td')[0].time['datetime']
      game = ""
      if row.css('td:nth-child(2) > a').present?
        game = row.css('td:nth-child(2) > a').text.strip
      else
        # No link tag, get the raw text before the div.visible-xs
        game = row.css('td:nth-child(2) > text()').text.strip
      end
      category = row.css('td')[2].text.strip
      player = row.css('td')[3].text.strip
      e = schedule.schedule_events.new
      e.name = "#{game} - #{player}"
      e.at = DateTime.parse(time)
      e.save!
    end
  end
end