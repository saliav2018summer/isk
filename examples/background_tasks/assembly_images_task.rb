require 'net/http'
require 'uri'
require 'open-uri'
require 'base64'
require_relative '../cli_helpers.rb'

class AssemblyImagesTask < BackgroundTask
  # Photocrew slide ids
  #Photocrew_ids = [434, 435, 436, 437, 438] # Summer
  Photocrew_ids = [654, 655, 656, 657, 658] # Winter

  def self.perform
    say 'Fetching latests assembly.galleria.fi pictures'

    # Use wget because it easily follows the redirects and gives proper
    # user-agent
    rss = `wget http://assembly.galleria.fi/kuvat/rss/ -O - 2>/dev/null`
    xml = Nokogiri::Slop(rss)

    slides_index = 0
    slides = Photocrew_ids
    skip = 0
    xml.rss.channel.item.each do |item|
      if skip > 0
        skip -= 1
        next
      end
      puts URI.parse(item.link.text + '?img=full').to_s
      tmp_file = Tempfile.new('gallery-picture')
      `wget --quiet -O #{tmp_file.path} #{URI.parse(item.link.text + '?img=full')}`

      width, height = `identify -format '%[fx:w]x%[fx:h]' #{tmp_file.path}`.split('x')
      say "#{width}x#{height} image.. aspect #{width.to_f / height.to_f}"

      unless ((width.to_f / height.to_f) - 1.5).abs < 0.01
        say "Rejected, wrong aspect ratio"
        tmp_file.unlink
        next
      end

      picture = File.read(tmp_file.path)
      tmp_file.unlink

      say "Using image #{item.link.text}"
      slide = Slide.find(slides[slides_index])
      svg = Nokogiri::XML(slide.svg_data) do |config|
        config.huge
      end
      encoded = 'data:image/jpeg;base64,' + Base64.encode64(picture)
      picture_element = svg.at_css('image#gallery_picture')
      picture_element['xlink:href'] = encoded

      File.open(slide.svg_filename, 'w+') do |f|
        f.puts svg.to_xml
      end
      picture_element = nil
      svg = nil
      slide.generate_images_later
      skip = 5
      slides_index+= 1
      picture = nil
      break if slides_index >= slides.size
    end
  end
end